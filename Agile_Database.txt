﻿CREATE DATABASE IF NOT EXISTS Agile;
USE Agile;

DROP TABLE IF EXISTS UserAdmin;

CREATE TABLE UserAdmin(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	Admin_Username VARCHAR(20),
	Admin_Password  VARCHAR (20)
	);
    
 INSERT INTO UserAdmin VALUES ( null, 'admin', 'pass');
   
    select * from UserAdmin;
    
    
DROP TABLE IF EXISTS UserStudent;

CREATE TABLE UserStudent(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	Student_Username VARCHAR(20),
	Student_Password  VARCHAR (20),
	Fines DECIMAL (9,2),
	Funds DECIMAL (9,2)
	);


DROP TABLE IF EXISTS Books;


CREATE TABLE Books(
	Book_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	Book_Name VARCHAR(20),
	Author  varchar (20),
    Library_Number  INTEGER (6),
	ISBN_Number INTEGER(6),
	Ref_Number  INTEGER (6),
    Release_Year  INTEGER (6)
	);
    
    
    
INSERT INTO Books VALUES ( null, 'Agile', 'Patrycja Boniakowska', 01, 001, 0001, 2017);

    
DROP TABLE IF EXISTS Journals;


CREATE TABLE Journals(
	Journal_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	Journal_Name VARCHAR(20),
	Author  varchar (20),
    Library_Number  INTEGER (20),
	ISBN_Number INTEGER(20),
	Ref_Number  INTEGER (20),
    Release_Year  INTEGER (20)
	);
    
    
INSERT INTO Journals VALUES ( null, 'JUnit', 'Patrycja Boniakowska', 01, 001, 0001, 2017);




 DROP TABLE IF EXISTS req_Book;
 
    CREATE TABLE req_Book (
	id INTEGER NOT NULL PRIMARY KEY,
	title VARCHAR(15) NOT NULL,
	author VARCHAR(20) NOT NULL,
	ISBN INTEGER NOT NULL,
	email VARCHAR (255) NOT NULL
);
    
    DROP TABLE IF EXISTS req_Journal;
    
    CREATE TABLE req_Journal (
	id INTEGER NOT NULL PRIMARY KEY,
	title VARCHAR(15) NOT NULL,
	author VARCHAR(20) NOT NULL,
	ISBN INTEGER NOT NULL,
	email VARCHAR (255) NOT NULL
);

Select * from Books;



